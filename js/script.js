var isMobile = false;
// device detection
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    isMobile = true;
    $('body').addClass('mob');
    $(".left-col").trigger("sticky_kit:detach");

} else {
    isMobile = false;
    $('body').removeClass('mob');
    // Sticky left col
    $(".left-col").stick_in_parent({
        offset_top: 0
    });
}
    var video = document.getElementById("video");
    if($('video').length ==1){
        if ( video.readyState === 4 ) {
            $('.video-bg').fadeIn();
        }
        video.addEventListener('loadeddata', function() {
           $('.video-bg').fadeIn();
        }, false);
}

$(document).ready(function() {
    // TABS
    $('.tabNavigation li a').click(function(event) {
        $(this).closest('li').addClass('active').siblings().removeClass('active');
        $(this).closest('.tabs').find('.tabs_box').hide().filter($($(this).attr('href'))).show();
        event.preventDefault();
    });

    // Common scripts
    $('.partners-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1100,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 800,
            settings: {
                slidesToShow: 1
            }
        }]
    });
    $('.reviews-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1
    });
  
    // More code link
    $('.more-code-link').toggle(function(e) {
        $(this).text('Свернуть');
        $(this).closest('.tabs_box').find('.code').addClass('full');
        e.preventDefault();
    }, function(e) {
        $(this).text('Развернуть весь код');
        $(this).closest('.tabs_box').find('.code').removeClass('full');
        e.preventDefault();
    });

    // Left nav anchors
    $('.left-nav li a').click(function(event) {
        var scrollTo = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(scrollTo).offset().top - 30
        }, 500);
        event.preventDefault();
    });

    /* Popup windows */
    if ( $('.js-open-popup').length ) {
        $('.js-open-popup').magnificPopup();
    }
});